# ПЗ під час розробки

## Завдання - git rebase, squash

1. Створити пустий гіт репозиторій і створити там 4 коміта:
    - first change
    - second change
    - third change
    - forth change
2. Змінити ім’я коміту “second change” на “second change (updated)”
3. Зклеїти коміти “third change” і “forth change” в один, назвати його “third and forth changes”.

В результаті мають вийти такі коміти:
- first change
- second change (updated)
- third and forth changes
